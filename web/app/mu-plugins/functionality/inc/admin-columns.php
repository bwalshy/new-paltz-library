<?php

/**
 * In the Posts Admin page (the list of posts), 
 * add 'Featured' as a column
 */
function add_posts_featured_column($columns) {
  $columns['is_featured'] = 'Featured';
  return $columns;
}
add_filter('manage_posts_columns', 'add_posts_featured_column');

/**
 * In the Posts Admin page, display the value of the featured field
 */
function display_posts_featured_column($column_name, $post_id) {
  if($column_name == 'is_featured') {

    $is_featured = get_field('post_is_featured', $post_id);

    if ($is_featured) {
      echo '<span class="dashicons dashicons-yes green" title="Yes"></span>';
    }
  }
}
add_action('manage_posts_custom_column', 'display_posts_featured_column', 10, 2);

/**
 * Add column styles to admin 
 */
function admin_columns_enqueue_style() {
  $path = plugins_url('assets/css/admin-columns.css', __DIR__ );
  wp_enqueue_style( 'admin_columns_style', $path, false  );
}

admin_columns_enqueue_style();

