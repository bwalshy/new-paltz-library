<?php
//add_action('acf/init', 'forms_and_requests');

forms_and_requests();

function forms_and_requests() {
  $page_check = get_page_by_path('formindex');                  // get page by slug

  if( function_exists('acf_add_local_field_group') && isset($page_check) ):
    $page_id = $page_check->ID;                                 // get its ID

    acf_add_local_field_group(array(
      'key' => 'group_5a3e022dc6fe9',
      'title' => 'Forms and Requests',
      'fields' => array(
        array(
          'key' => 'field_5a45a2f7c82b4',
          'label' => 'Primary Forms and Requests',
          'name' => 'primary_forms_and_requests',
          'type' => 'repeater',
          'instructions' => 'This section appears first and the links shown here are bigger. More emphasis is placed on this section.',
          'required' => 0,
          'conditional_logic' => 0,
          'wrapper' => array(
            'width' => '',
            'class' => '',
            'id' => '',
          ),
          'collapsed' => '',
          'min' => 0,
          'max' => 0,
          'layout' => 'table',
          'button_label' => '',
          'sub_fields' => array(
            array(
              'key' => 'field_5a45a322c82b5',
              'label' => 'Links',
              'name' => 'primary_forms_and_requests_link',
              'type' => 'link',
              'instructions' => '',
              'required' => 0,
              'conditional_logic' => 0,
              'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
              ),
              'return_format' => 'array',
            ),
          ),
        ),
        array(
          'key' => 'field_5a3e03fe0ca97',
          'label' => 'Sections',
          'name' => 'forms_and_requests_sections',
          'type' => 'repeater',
          'instructions' => 'Create a set of links underneath sections you create. I.e. "Purchase Requests", "For Faculty and Staff", "Donations", etc.',
          'required' => 0,
          'conditional_logic' => 0,
          'wrapper' => array(
            'width' => '',
            'class' => '',
            'id' => '',
          ),
          'collapsed' => '',
          'min' => 0,
          'max' => 0,
          'layout' => 'row',
          'button_label' => '',
          'sub_fields' => array(
            array(
              'key' => 'field_5a3e042a0ca98',
              'label' => 'Section Name',
              'name' => 'forms_and_requests_section_name',
              'type' => 'text',
              'instructions' => '',
              'required' => 0,
              'conditional_logic' => 0,
              'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
              ),
              'default_value' => '',
              'placeholder' => '',
              'prepend' => '',
              'append' => '',
              'maxlength' => '',
            ),
            array(
              'key' => 'field_5a3e05441bb2a',
              'label' => 'Links to Forms and Requests',
              'name' => 'forms_and_requests_links',
              'type' => 'repeater',
              'instructions' => '',
              'required' => 0,
              'conditional_logic' => 0,
              'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
              ),
              'collapsed' => '',
              'min' => 0,
              'max' => 0,
              'layout' => 'row',
              'button_label' => 'Add Link',
              'sub_fields' => array(
                array(
                  'key' => 'field_5a3e043a0ca99',
                  'label' => 'Link',
                  'name' => 'form_and_request_link',
                  'type' => 'link',
                  'instructions' => '',
                  'required' => 0,
                  'conditional_logic' => 0,
                  'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                  ),
                  'return_format' => 'array',
                ),
              ),
            ),
          ),
        ),
      ),
      'location' => array(
        array(
          array(
            'param' => 'page',
            'operator' => '==',
            'value' => $page_id,
          ),
        ),
      ),
      'menu_order' => 0,
      'position' => 'normal',
      'style' => 'default',
      'label_placement' => 'top',
      'instruction_placement' => 'label',
      'hide_on_screen' => array(
        0 => 'the_content',
      ),
      'active' => 1,
      'description' => '',
    ));
endif;
}
