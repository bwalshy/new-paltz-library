<?php
/**
 *
 * Custom field utilities for Advanced Custom Fields (ACF)
 *
 * ACF config data will be saved and loaded from JSON files. This makes it 
 * so custom field schemas can be versioned in git, instead of manually added
 * to the database on every environment.
 *
 * The server needs to be able to read and write from this file (755 should suffice)
 */

namespace NPL\CustomFields;

/**
 * Advanced Custom Fields Site Options
 */
if( function_exists('acf_add_options_page')  ) {
  acf_add_options_page(array(
    'page_title'  => 'Site Settings',
    'menu_title'  => 'Site Settings',
    'menu_slug'   => 'theme-general-settings',
    'capability'  => 'edit_posts',
    'redirect'    => false
  ));
}

// Load Home Page Featured Posts options
include_once( plugin_dir_path( __FILE__ ) . 'custom-fields/home_page_featured_posts.php' );

// Load About the Library Modules
include_once( plugin_dir_path( __FILE__ ) . 'custom-fields/about_the_library.php' );

// Load Forms and Requests fields
include_once( plugin_dir_path( __FILE__ ) . 'custom-fields/forms_and_requests.php' );
