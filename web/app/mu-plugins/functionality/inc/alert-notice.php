<?php
// alert notice
// Notification bar
// Note: potentially place this in a `plugins` folder in in mu-plugins/functionality/plugins or somewhere along those lines

class NotificationBar
{
  /**
   * Holds the values to be used in the fields callbacks
   */
  private $options;

  /**
   * Start up
   */
  public function __construct()
  {
    add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
    add_action( 'admin_init', array( $this, 'page_init' ) );
  }

  /**
   * Add options page
   */
  public function add_plugin_page()
  {
    // This page will be under "Settings"
    $hook = add_menu_page(
      'Notification Bar Admin',
      'Notification Bar',
      'manage_options',
      'notification-bar',
      array( $this, 'create_admin_page' ),
      //array( $this, 'create_notification_bar_page' ),
      'dashicons-warning'
    );

    // After page load, call `on_page_load`
    add_action('load-'.$hook, array( $this, 'on_page_load' ));
  }

  /**
   * Set the WP Cron Job to turn off the Notification Bar after the alloted time
   * has passed.
   *
   * The action, 'deactivate_notification_bar', is defined in functions.php
   */
  public function on_page_load()
  {

    // If the options page has been updated, set a WP cron job to
    // turn off the notification bar
    if ( isset( $_GET['settings-updated'] )) {
      $options = get_option( 'notification_bar_name' );

      if ($options['end_time']) {
        // Clear any previously scheduled jobs
        wp_clear_scheduled_hook( 'deactivate_notification_bar'  );

        // Set a new job at the specified end time
        wp_schedule_single_event( strtotime($options['end_time']), 'deactivate_notification_bar' );
      }
    }
  }

  /**
   * Options page callback
   */
  public function create_admin_page()
  {
    // Set class property
    $this->options = get_option( 'notification_bar_name' );
    ?>
      <div class="wrap notification-bar-admin-wrapper">
        <h1>Notification Bar</h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam elementum lobortis leo eget semper. Aenean odio lorem, ultrices vitae purus in, pellentesque aliquam urna. Aliquam accumsan vehicula justo, tempor elementum nibh. Maecenas elementum arcu risus, et tristique elit porttitor at. Maecenas vel interdum tortor. Interdum et malesuada fames ac ante ipsum primis in faucibus. Mauris auctor risus a faucibus ultrices.</p>
        <form method="post" action="options.php">
        <?php
          // This prints out all hidden setting fields
          settings_fields( 'notification_bar_group' );
          do_settings_sections( 'my-setting-admin' );
          $this->render_preview();
          submit_button();
        ?>
        </form>
      </div>
  <?php
  }

  /**
   * Render title, and form (notification bar options)
   */
  public function create_notification_bar_page()
  {
    ?>
      <div class="wrap notification-bar-admin-wrapper">
        <h1>Notification Bar</h1>
        <p class="notification-bar__instructions">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam elementum lobortis leo eget semper. Aenean odio lorem, ultrices vitae purus in, pellentesque aliquam urna. Aliquam accumsan vehicula justo, tempor elementum nibh. Maecenas elementum arcu risus, et tristique elit porttitor at. Maecenas vel interdum tortor. Interdum et malesuada fames ac ante ipsum primis in faucibus. Mauris auctor risus a faucibus ultrices.</p>

        <form id="notification-bar__form" method="post" action="">

          <div class="notification-bar__field-group">
            <label for="notification-bar-active" class="primary-label">Activate the notification bar?</label>
            <label class="activate-switch">
              <input type="checkbox" name="notification-bar-active"/>
              <span class="slider"></span>
            </label>
          </div>

          <div class="notification-bar__field-group">
            <label for="notification-bar-alert-title" class="primary-label">Alert Title:</label>
            <input type="text" class="active-switch-dependent" name="notification-bar-alert-title"/>
          </div>

          <div class="notification-bar__field-group">
            <label for="notification-bar-alert-message" class="primary-label">Alert Message:</label>
            <textarea class="active-switch-dependent" name="notification-bar-alert-message"></textarea>
          </div>

          <div class="notification-bar__field-group">
            <label for="notification-bar-alert-color" class="primary-label">Alert Color:</label>
            <div class="notification-bar__color">
              <input type="radio" class="active-switch-dependent" name="notification-bar-alert-color" value="#B21E1E" id="notification-bar__alert-color-red"/>
              <label for="notification-bar__alert-color-red"><span class="notification-bar__alert-color-red"></span></label>

              <input type="radio" class="active-switch-dependent" name="notification-bar-alert-color" value="#CD9903" id="notification-bar__alert-color-yellow"/>
              <label for="notification-bar__alert-color-yellow"><span class="notification-bar__alert-color-yellow"></span></label>

              <input type="radio" class="active-switch-dependent" name="notification-bar-alert-color" value="#1E73BE" id="notification-bar__alert-color-blue"/>
              <label for="notification-bar__alert-color-blue"><span class="notification-bar__alert-color-blue"></span></label>
            </div>
          </div>

          <div class="notification-bar__field-group">
            <label class="primary-label">Timeframe:</label>
            <label for="notification-bar-start-time">Start Time:</label>
            <input class="datetimepicker active-switch-dependent" type="text"/>

            <label for="notification-bar-end-time">End Time:</label>
            <input class="datetimepicker active-switch-dependent" type="text"/>
          </div>

          <p class="submit">
            <input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes">
          </p>

        </form>
      </div>
    <?php
  }

  /**
   * Register and add settings
   */
  public function page_init()
  {
    register_setting(
      'notification_bar_group', // Option group
      'notification_bar_name' // Option name
      //array( $this, 'sanitize' ) // Sanitize
    );

    add_settings_section(
      'setting_section_id', // ID
      'Settings', // Title
      array( $this, 'print_section_info' ), // Callback
      'my-setting-admin' // Page
    );

    add_settings_field(
      'active', // ID
      'Activate the notification bar?', // Title
      array( $this, 'print_activation_checkbox' ), // Callback
      'my-setting-admin', // Page
      'setting_section_id' // Section
    );

    add_settings_field(
      'alert_title', // ID
      'Alert Title', // Title
      array( $this, 'print_alert_title' ), // Callback
      'my-setting-admin', // Page
      'setting_section_id' // Section
    );

    add_settings_field(
      'alert_message', // ID
      'Alert Message', // Title
      array( $this, 'print_alert_message' ), // Callback
      'my-setting-admin', // Page
      'setting_section_id' // Section
    );

    add_settings_field(
      'alert_color', // ID
      'Alert Color', // Title
      array( $this, 'print_alert_color' ), // Callback
      'my-setting-admin', // Page
      'setting_section_id' // Section
    );

    add_settings_field(
      'start_time', // ID
      'Start Time', // Title
      array( $this, 'print_start_time' ), // Callback
      'my-setting-admin', // Page
      'setting_section_id' // Section
    );

    add_settings_field(
      'end_time', // ID
      'End Time', // Title
      array( $this, 'print_end_time' ), // Callback
      'my-setting-admin', // Page
      'setting_section_id' // Section
    );

  }

  /**
   * Sanitize each setting field as needed
   *
   * @param array $input Contains all settings fields as array keys
   */
  public function sanitize( $input )
  {
    $new_input = array();
    if( isset( $input['id_number'] ) )
      $new_input['id_number'] = absint( $input['id_number'] );

    if( isset( $input['title'] ) )
      $new_input['title'] = sanitize_text_field( $input['title'] );

    return $new_input;
  }

  /**
   * Print the Section text
   */
  public function print_section_info()
  {
    print 'Enter your settings below:';
  }

  /**
   * Get the settings option array and print one of its values
   */
  public function id_number_callback()
  {
    printf(
      '<input type="text" id="id_number" name="notification_bar_name[id_number]" value="%s" />',
      isset( $this->options['id_number'] ) ? esc_attr( $this->options['id_number']) : ''
    );
  }

  /**
   * Print switch that turns the notification bar on or off
   */
  public function print_activation_checkbox()
  {
    printf(
      '<div class="notification-bar__field-group">
        <label class="activate-switch">
          <input type="checkbox" name="notification_bar_name[active]" %s/>
          <span class="slider"></span>
        </label>
      </div>',
      isset( $this->options['active'] ) ? 'checked' : ''
    );
  }

  public function print_alert_title() 
  {
    printf(
      '<div class="notification-bar__field-group">
        <input type="text" class="active-switch-dependent" value="%s" name="notification_bar_name[alert_title]"/>
      </div>',

      isset( $this->options['alert_title'] ) ? esc_attr( $this->options['alert_title']) : ''
    );
  }

  public function print_alert_message()
  {
    $settings = array('textarea_name' => 'notification_bar_name[alert_message]');
    printf(
      wp_editor(
        isset( $this->options['alert_message'] ) ? $this->options['alert_message'] : '',
        'alertmessage',
        $settings
      )
    );
  }

  public function render_preview()
  {
    $options = get_option('notification_bar_name');
    ?>
      <h2>Preview:</h2>
      <div class="notification-bar" style="background-color: <?php echo isset($options['alert_color']) ? $options['alert_color'] : '#CD9903' ?>">
        <?php echo isset($options['alert_title']) ? '<h2>' . $options['alert_title'] .'</h2>' : '' ?>
        <?php echo isset($options['alert_message']) ? '<p>' . $options['alert_message'] .'</p>' : '' ?>
      </div>
    <?php
  }

  public function print_alert_color()
  {
    ?>
      <div class="notification-bar__color">
        <input
          type="radio"
          class="active-switch-dependent"
          name="notification_bar_name[alert_color]"
          value="#B21E1E"
          id="notification-bar__alert-color-red"
          <?php // if 'alert_color' exists AND its value is red, add 'checked' to the input field ?>
          <?= isset($this->options['alert_color']) && $this->options['alert_color'] == "#B21E1E" ? "checked" : "" ?>
        />
        <label for="notification-bar__alert-color-red"><span class="notification-bar__alert-color-red"></span></label>

        <input
          type="radio"
          class="active-switch-dependent"
          name="notification_bar_name[alert_color]"
          value="#CD9903"
          id="notification-bar__alert-color-yellow"
          <?php // if 'alert_color' exists AND its value is yellow, add 'checked' to the input field ?>
          <?= isset($this->options['alert_color']) && $this->options['alert_color'] == "#CD9903" ? "checked" : "" ?>
        />
        <label for="notification-bar__alert-color-yellow"><span class="notification-bar__alert-color-yellow"></span></label>

        <input
          type="radio"
          class="active-switch-dependent"
          name="notification_bar_name[alert_color]"
          value="#1E73BE"
          id="notification-bar__alert-color-blue" 
          <?php // if 'alert_color' exists AND its value is below, add 'checked' to the input field ?>
          <?= isset($this->options['alert_color']) && $this->options['alert_color'] == "#1E73BE" ? "checked" : "" ?>
        />
        <label for="notification-bar__alert-color-blue"><span class="notification-bar__alert-color-blue"></span></label>
      </div>
    <?php
  }

  public function print_start_time()
  {
    printf(
      '<input class="datetimepicker active-switch-dependent" value="%s" name="notification_bar_name[start_time]" type="text"/>',
      isset( $this->options['start_time'] ) ? esc_attr( $this->options['start_time']) : ''
    );
  }

  public function print_end_time()
  {
    printf(
      '<input class="datetimepicker active-switch-dependent" value="%s" name="notification_bar_name[end_time]" type="text"/>',
      isset( $this->options['end_time'] ) ? esc_attr( $this->options['end_time']) : ''
    );
  }

  /**
   * Get the settings option array and print one of its values
   */
  public function title_callback()
  {
    printf(
      '<input type="text" id="title" name="notification_bar_name[title]" value="%s" />',
      isset( $this->options['title'] ) ? esc_attr( $this->options['title']) : ''
    );
  }
}

if( is_admin() )
  $my_settings_page = new NotificationBar();
