<?php
/**
 * Rest API
 */

namespace NPL\RestApi; 


add_action( 'rest_api_init', function () {
  register_rest_route( 'api/v1', '/library-hours', array(
    'methods' => 'GET',
    'callback' => __NAMESPACE__ . '\\library_hours',
  ));
});

/**
 * Fetch library hours from 'LibraryHoursTable.txt' file
 * and return result as a JSON object
 * @param  WP_REST_Request $request
 * @return Array   site meta data struct
 */
function library_hours(\WP_REST_Request $request) {

  $qparams = $request->get_query_params();

  // n is a numeric representation of a month (1-12) without leading 0
  // j is a numeric representation of a day (1-31) without leading 0
  $today = date('n/j/Y');

  // to keep track of which index marks
  // where to splice the allhours array 
  // to only get the next 7 days
  $starting_days_index = 1;

  // Open the text file and get the content
  $filename = "https://library.newpaltz.edu/LibraryHoursTable.txt";
  $data   = file_get_contents($filename);
  $rowsArr  = explodeRows($data);

  // Generate objects
  for($i = 0; $i < (count($rowsArr)-1); $i = $i +1){

    $lineDetails = explodeTabs($rowsArr[$i]);
    $date = trim($lineDetails[1]);

    $allhours[$i]["linenum"] = trim($lineDetails[0]);
    $allhours[$i]["date"] = $date;
    $allhours[$i]["daylabel"] = trim($lineDetails[2]);
    $allhours[$i]["dayow"] = trim($lineDetails[3]);
    $allhours[$i]["open"] = trim($lineDetails[4]);
    $allhours[$i]["close"] = trim($lineDetails[5]);
    $allhours[$i]["refopen"] = trim($lineDetails[6]);
    $allhours[$i]["refclose"] = trim($lineDetails[7]);
    $allhours[$i]["latenight"] = trim($lineDetails[8]);

    // if we are at today's date,
    // store the index in startingDaysIndex
    if($today === $date) {
      $starting_days_index = $i;
    }
  }

  // If all_hours url query parameter was set,
  // return all hours
  if (isset($qparams['all_hours']) && !empty($qparams['all_hours'])) {
    return $allhours;
  }

  // If today_only url query parameter was set,
  // return only today's hours 
  if (isset($qparams['only_today']) && !empty($qparams['only_today'])) {
    return array_slice($allhours, $starting_days_index, 1);
  }

  // By default, only next 7 days (including today)
  // Splice the allhours array to only
  // get the next 7 days

  return array_slice($allhours, $starting_days_index, 7);
}



/* Utility / helper functions */

function explodeRows($data) {
  $rowsArr = explode("\n", $data);
  return $rowsArr;

}

// Explode the columns according to tabs
function explodeTabs($singleLine) {
  $tabsArr = explode("\t", $singleLine);
  return $tabsArr;
}
