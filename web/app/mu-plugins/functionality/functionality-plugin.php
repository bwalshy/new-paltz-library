<?php
/*
  Plugin Name: ~New Paltz Library Standard Functionality
  Description: This handles non-theme-specific functionality for this website. In here are custom post type (CPT) and taxonomy (taxo) definitions, and Gravity Forms hooks
  Version: 1.0
  Author: Brandon Walsh
 */

  add_action( 'init', 'npl_init' );

/**
 * Wordpress 'init' action hook.
 * Runs after WordPress has finished loading but before any headers are sent. Useful for intercepting $_GET or $_POST triggers
 * N.B.: Also load_plugin_textdomain calls should be made during init, otherwise users cannot hook into it.
 */
function npl_init() {
  // register CPTs, taxos, etc.
  npl_register_post_types_taxos();

  // register Admin Columns
  npl_register_admin_columns();

  // register custom fields
  npl_register_custom_fields();

  // register WP Rest API routes
  npl_register_rest_api();

  // register alert notice plugin
  npl_register_alert_notice();

}

/**
 * Register CPTs and taxos
 */
function npl_register_post_types_taxos() {
  //include_once( plugin_dir_path( __FILE__  ) . 'inc/posttypes-taxos.php'  );
}

/**
 * Register Admin Columns
 */
function npl_register_admin_columns() {
  include_once( plugin_dir_path( __FILE__  ) . 'inc/admin-columns.php' );
}

/**
 * Register custom fields
 */
function npl_register_custom_fields() {
  include_once( plugin_dir_path( __FILE__  ) . 'inc/custom-fields.php' );
}

/**
 * Register Rest API
 */
function npl_register_rest_api() {
  include_once( plugin_dir_path( __FILE__  ) . 'inc/rest-api.php'  );
}

/**
 * Register Alert Notice Plugin
 */
function npl_register_alert_notice() {
  include_once( plugin_dir_path( __FILE__  ) . 'inc/alert-notice.php'  );
}
