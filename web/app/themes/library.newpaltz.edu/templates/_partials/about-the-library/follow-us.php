<?php
  $facebook = get_field('follow_us_facebook');
  $twitter = get_field('follow_us_twitter');
  $instagram = get_field('follow_us_instagram');
  $hashtag = get_field('follow_us_hashtag');
?>

<?php if($facebook): ?>
  <li><a href="<?= $facebook ?>"><img src="<?= get_template_directory_uri() ?>/dist/images/icons/facebook.png" alt="Facebook logo"/></a></li>
<?php endif; ?>

<?php if($twitter): ?>
  <li><a href="<?= $twitter ?>"><img src="<?= get_template_directory_uri() ?>/dist/images/icons/twitter.png" alt="Twitter logo"/></a></li>
<?php endif; ?>

<?php if($instagram): ?>
  <li><a href="<?= $instagram ?>"><img src="<?= get_template_directory_uri() ?>/dist/images/icons/instagram.png" alt="Instagram logo"/></a></li>
<?php endif; ?>

<?php if($hashtag): ?>
  <li><a href="<?= $hashtag?>">#NPLibrary</a></li>
<?php endif; ?>
