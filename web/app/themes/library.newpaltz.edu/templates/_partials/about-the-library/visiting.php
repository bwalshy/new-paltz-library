<?php
  $visiting_links = fetch_sub_field_links('visiting_repeater');
?>

<?php foreach($visiting_links as &$link): ?>
  <li>
    <a class="standalone-link" href="<?= $link['url'] ?>" target="<?= $link['target']?>">
      <?=$link['title'] ?>
    </a>
  </li>
<?php unset($link);endforeach; ?>
