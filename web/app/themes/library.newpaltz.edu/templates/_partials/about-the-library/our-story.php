<?php
  $our_story_links = fetch_sub_field_links('our_story_repeater');
?>

<?php foreach($our_story_links as &$link): ?>
  <li>
    <a class="standalone-link" href="<?= $link['url'] ?>" target="<?= $link['target']?>">
      <?=$link['title'] ?>
    </a>
  </li>
<?php unset($link);endforeach; ?>
