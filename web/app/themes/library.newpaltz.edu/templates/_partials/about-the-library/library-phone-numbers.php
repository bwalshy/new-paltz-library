<?php
  $repeater_field_name = 'library_department_phone_repeater';

  if( have_rows($repeater_field_name) ):

    // for every row in the repeater
    while( have_rows($repeater_field_name)  ): the_row(); 

      // get the link
      $phone_name = get_sub_field('phone_numbers_number_name');
      $phone_number = get_sub_field('phone_numbers_number');
      $phone_number_display = get_sub_field('phone_numbers_number_display');
      $fax_name = get_sub_field('phone_numbers_fax_name');
      $fax_number = get_sub_field('phone_numbers_fax_number');
?>  

<li>
  <div class="two-column">
    <div>
      <span><?= $phone_name ?></span>
    </div>
    <div>
      <a href="tel:<?= $phone_number ?>"><?= $phone_number_display ?></a>
      
      <?php if($fax_name): ?>
        <br>
        <a href="<?= $fax_number ?>"><?= $fax_name ?></a>
      <?php endif; ?>
    </div>
  </div>
</li>

<?php endwhile; endif; ?>
