<?php
  $other_contacts_links = fetch_sub_field_links('other_contacts_repeater');
?>

<?php foreach($other_contacts_links as &$link): ?>

  <li>
    <a href="<?= $link['url'] ?>" target="<?= $link['target']?>"><?=$link['title'] ?></a>
  </li>
<?php unset($link);endforeach; ?>
