<?php
$module_title = 'Help & Assistance';
$acf_blurb = get_field('help_and_assistance_blurb');
$blurb = $acf_blurb ? $acf_blurb : 'Having trouble? The librarians at STL are eager to help! Ask a librarian a question or use our thorough FAQ.';
?>
<div class="module-component component help-and-assistance-module">
  <div class="module-heading">
    <?= $module_title ?>
  </div>

  <div class="module-content">
    <p><?= $blurb ?></p>
    <div class="two-column">
      <div>
        <?php if(have_rows('help_and_assistance_repeater')): ?>
          <?php while (have_rows('help_and_assistance_repeater')) : the_row(); $link = get_sub_field('help_and_assitance_repeater_link'); ?>
            <div class="button small">
              <a href="<?= $link['url'] ?>" target="<?= $link['target']?>"><?=$link['title'] ?></a>
            </div>
          <?php endwhile; ?>
        <?php endif; ?>
      </div>
      <div>
        <div class="help-button-container">
          <div class="help-button">
            <a href="https://newpaltz.libanswers.com/">Help</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
