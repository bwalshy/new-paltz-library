<div class="modal component library-traffic-modal">
  <div class="two-column">
    <div class="library-traffic-library-hours">
      <h3>The Next 7 Days</h3>
      <div class="table-container">
        <table>
          <caption>Library Hours for the next 7 Days:</caption>
          <thead>
            <tr>
              <th scope="col">Weekday</th>
              <th scope="col">Library Hours</th>
              <th scope="col">Research Desk</th>
              <th scope="col">Late Room</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
      <div class="two-column">
        <div class="button small">
          <a href="https://library.newpaltz.edu/libcalendar.html">Full Calendar</a>
        </div>
        <div class="button small">
          <a href="https://library.newpaltz.edu/library_hours.html">Holidays &amp; Exceptions</a>
        </div>
      </div>
    </div>

    <div>
      <h3>Computer Availability</h3>

      <div class="availability-tracker-container library-traffic-pc-availability">
        <div class="availability-tracker" id="availability-tracker-desktop">
          <div class="availability-tracker-heading">
            <span><a href="/pctracker">Desktop (80 Total)</a></span>
          </div>
          <div class="availability-tracker-data">
            <div class="availability-tracker-visualization">
            </div>
            <div class="availability-tracker-numbers">
              <div class="number-available-container">
                <div class="number-available"></div> available
              </div>
              <div class="number-in-use-container">
                <div class="number-in-use"></div> in use
              </div>
            </div>
          </div>
        </div>
        <div class="availability-tracker" id="availability-tracker-laptop">
          <div class="availability-tracker-heading">
            <span><a href="/pctracker">Laptop (32 Total)</a></span>
          </div>
          <div class="availability-tracker-data">
            <div class="availability-tracker-visualization">
            </div>
            <div class="availability-tracker-numbers">
              <div class="number-available-container">
                <div class="number-available"></div> available
              </div>
              <div class="number-in-use-container">
                <div class="number-in-use"></div> in use
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal-close-button">
    <a href="javascript:void(0);" aria-label="Close Library Hours Modal">
      <img src="<?= get_template_directory_uri() ?>/dist/images/icons/close-button.svg" alt="Close Library Hours"/>
    </a>
  </div>
</div>
