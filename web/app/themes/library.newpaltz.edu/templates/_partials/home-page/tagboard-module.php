<?php
$np_social_module = get_field('np_social_module')[0];               // get np social module
$np_module_title = $np_social_module['np_social_module_title'];     // grab the module title field

$module_title = $np_module_title ? $np_module_title : '#npsocial';  // set default option if title field was empty
wp_reset_query();                                                   // reset query in case we use `get_field` again
?>

<div class="module-component tagboard-module component scrollable" aria-hidden="true">
  <div class="module-heading">
    <?= $module_title ?>
  </div>

  <div class="module-content">
    <a href="#footer" class="skip-tagboard">Skip Tagboard Feed</a>
    <div class="tagboard-embed" tgb-slug="t/279857" tgb-layout="waterfall" tgb-feed-type="default" tgb-post-count="2" tgb-mobile-count="1" tgb-toolbar="none" tgb-animation-type="default" tgb-hashtag-color="003E7E" tgb-fixed-height="true" tgb-media-only="true"></div>
    <script src="//static.tagboard.com/public/js/embedAdvanced.js"></script>
  </div>
</div>
