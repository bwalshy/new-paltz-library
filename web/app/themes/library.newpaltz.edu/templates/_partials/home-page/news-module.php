<div class="module-component component module-carousel news-module">
  <div class="module-heading">
    Check it Out!
  </div>

  <?php

  // get featured posts
  $featured_posts = get_field('home_featured_posts');
  $features = array_slice($featured_posts, 0, 4);
  $i = 1;


  // loop posts, get their relevant attributes, and display them
  foreach($features as $post):

    $post_id = $post['home_featured_post'];
    $post_date = get_the_date(null, $post_id, false);

  /*
   * Use the ternary operator to retrieve attributes from the current post, or
   * use custom values if "home_featured_customized_display" was selected and if they are not blank
   */
  $post_title = ($post['home_featured_customized_display'] && !empty($post['home_featured_post_title'])) 
  ? $post['home_featured_post_title'] : get_the_title($post_id);

  $post_excerpt = ($post['home_featured_customized_display'] && !empty($post['home_featured_post_excerpt']))
  ? $post['home_featured_post_excerpt'] : get_the_excerpt($post_id);

  $post_image_id = ($post['home_featured_customized_display'] && !empty($post['home_featured_post_image']))
  ? $post['home_featured_post_image'] : get_post_thumbnail_id($post_id);

  $post_image_url = (!empty($post_image_id))? wp_get_attachment_image_url($post_image_id, 'large') : false;

  $permalink = ($post['home_featured_customized_display'] && !empty($post['home_featured_post_link']))
  ? $post['home_featured_post_link'] : get_permalink($post_id);


  ?>

    <div class="module-content <?= $i === 1 ? 'active' : '' ?>" data-slide-index="<?= $i ?>">
      <a href="<?= $permalink ?>" aria-hidden="true" alt="<?= $post_title ?>">
        <?php if($post_image_url): ?>
          <div class="module-image" role="presentation" style="background-image:url(<?= $post_image_url ?>)"></div>
        <?php endif; ?>
        <h3><?= $post_title ?></h3>
      </a>

    <p><?= $post_excerpt ?></p>
  </div>

  <?php $i++; endforeach; wp_reset_query(); ?>

  <div class="module-carousel-nav">
    <div class="module-carousel-item-prev module-carousel-nav-item">
      <a href="javascript:void(0);"><span class="sr-only">View Previous Featured Post</span><img alt="View Previous Featured Post" src="<?= get_template_directory_uri() ?>/dist/images/icons/chevron-left.svg" aria-hidden="true"/></a>
    </div>

    <div class="module-carousel-item-next module-carousel-nav-item">
      <a href="javascript:void(0);"><span class="sr-only">View Next Featured Post</span><img alt="View Next Featured Post" src="<?= get_template_directory_uri() ?>/dist/images/icons/chevron-right.svg" aria-hidden="true"/></a>
    </div>
  </div>
</div>
