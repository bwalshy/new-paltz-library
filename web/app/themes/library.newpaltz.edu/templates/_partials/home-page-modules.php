<section class="events">
  <div class="container large">
    <div class="two-column">
      <div>
        <?php include('home-page/news-module.php'); ?>
      </div>
      <div>
        <?php include('home-page/help-and-assistance-module.php'); ?>
        <?php include('home-page/tagboard-module.php'); ?>
      </div>
    </div>
  </div>
</section>
