<?php use Roots\Sage\Titles; ?>

<div class="page-header h1-container">
  <h1><?= Titles\title(); ?></h1>
  <?php include_once('_partials/help-button.php'); ?>
</div>
