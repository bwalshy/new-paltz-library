<?php
/**
 * Template Name: Forms and Requests
 */

$sections = get_field('forms_and_requests_sections');

$column1 = array();
$column2 = array();
$i = 0;

// split sections into columns
if( have_rows('forms_and_requests_sections') ) {
  while( have_rows('forms_and_requests_sections') ) {
    the_row();

    $section_name = get_sub_field('forms_and_requests_section_name');
    $links = array();

    get_sub_field('forms_and_requests_links');
    if( have_rows('forms_and_requests_links') ) {
      while( have_rows('forms_and_requests_links') ) {
        the_row();
        array_push($links, get_sub_field('form_and_request_link'));
      }
    }

    $section = array(
      'section_name' => $section_name,
      'links' => $links 
    );

    if( $i % 2 === 0  ) {
      array_push($column1, $section);
    } else {
      array_push($column2, $section);
    }

    $i++;
  }
}
?>

<div class="forms-and-requests">
  <div class="h1-container">
    <h1>Forms and Requests</h1>
    <div class="help-button-container">
      <div class="help-button">
        <a href="http://newpaltz.libanswers.com">Help</a>
      </div>
    </div>
  </div>

  <div class="button-list">
    <ul class="no-dots">
      <?php
        if( have_rows('primary_forms_and_requests') ):
          while( have_rows('primary_forms_and_requests') ):
          the_row();
          $link = get_sub_field('primary_forms_and_requests_link');
      ?>
      <li>
        <a class="standalone-link" href="<?= $link['url'] ?>" target="<?= $link['target']?>"><?=$link['title'] ?></a>
      </li>
      <?php endwhile; endif; ?>
    </ul>
  </div>

  <div class="two-column">
    <div>
      <?php foreach($column1 as $section): ?>
        <div class="module-component component">
          <div class="module-heading">
            <?= $section['section_name'] ?>        
          </div>

          <div class="module-content">
            <ul>
              <?php foreach($section['links'] as &$link): ?>
                <li>
                  <a href="<?= $link['url'] ?>" target="<?= $link['target']?>"><?=$link['title'] ?></a>
                </li>
              <?php endforeach; ?>
            </ul>
          </div>
        </div>
      <?php endforeach; ?>
    </div>
  <div>
    <?php foreach($column2 as $section): ?>
      <div class="module-component component">
        <div class="module-heading">
          <?= $section['section_name'] ?>        
        </div>

        <div class="module-content">
          <ul>
            <?php foreach($section['links'] as &$link): ?>
              <li>
                <a href="<?= $link['url'] ?>" target="<?= $link['target']?>"><?=$link['title'] ?></a>
              </li>
            <?php endforeach; ?>
          </ul>
        </div>
      </div>
    <?php endforeach; ?>
  </div>
</div>
