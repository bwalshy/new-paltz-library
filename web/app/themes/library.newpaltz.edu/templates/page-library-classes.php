<?php
/**
 * Template Name: Library Classes
 */
?>
<div class="library-classes">
  <div class="h1-container">
    <h1>Library Classes</h1>
    <div class="help-button-container">
      <div class="help-button">
        <a href="http://newpaltz.libanswers.com">Help</a>
      </div>
    </div>
  </div>

  <iframe frameborder="0" height="600" scrolling="no" src="//www.google.com/calendar/embed?mode=AGENDA&amp;height=600&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src=stlinstruct%40gmail.com&amp;color=%23A32929&amp;ctz=America%2FNew_York" style=" border-width:0 " width="750"></iframe>
</div>
