<footer id="footer" class="content-info">
  <div class="resources-container">
    <div class="resources-dropdown dropdown">
      <?php
        if (has_nav_menu('footer_links')) :
          wp_nav_menu(['theme_location' => 'footer_links', 'menu_class' => 'resources_nav']);
        endif;
      ?>
      <button class="dropdown-button"><span>Directions </span><img alt=" " src="<?= get_template_directory_uri() ?>/dist/images/icons/triangle-down.svg" aria-hidden="true"/></button>
      <div class="dropdown-content">
        <?php
          if (has_nav_menu('directions_navigation')) :
            wp_nav_menu(['theme_location' => 'directions_navigation', 'menu_class' => 'directions_nav']);
          endif;
        ?>
      </div>
    </div>
  </div>
  <div class="footer-content">
    <div class="footer-logo-container">
      <div class="footer-logo">
        <img alt="NP Library Logo" src="<?= get_template_directory_uri() ?>/dist/images/suny-system-logo.svg"/>
      </div>
    </div>
    <div class="address">
      <p>
        <strong>Sojourner Truth Library</strong><br/>
        State University of New York New Paltz<br/>
        300 Hawk Drive, New Paltz, NY<br/>
        12561-2493
      </p>
    </div>
  </div>
</footer>
