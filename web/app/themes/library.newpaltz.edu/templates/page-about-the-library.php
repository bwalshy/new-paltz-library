<?php
/**
 * Template Name: About the Library
 */
?>

<div class="about-the-library">
  <div class="h1-container">
    <h1>About the Library</h1>
    <div class="help-button-container">
      <div class="help-button">
        <a href="http://newpaltz.libanswers.com">Help</a>
      </div>
    </div>
  </div>

  <div class="two-column">
    <div>
      <h2>Our Story</h2>
      <div class="button-list">
        <ul class="no-dots">
          <?php include_once('_partials/about-the-library/our-story.php'); ?>
        </ul>
      </div>
    </div>

    <div>
      <h2>Visiting</h2>
      <div class="button-list">
        <ul class="no-dots">
          <?php include_once('_partials/about-the-library/visiting.php'); ?>
        </ul>
      </div>
    </div>
  </div>

  <div class="two-column">
    <div>
      <div class="module-component component">
        <div class="module-heading">
          Library Department Phone Numbers
        </div>

        <div class="module-content">
          <ul class="library-phone-numbers-links no-dots">
            <?php include_once('_partials/about-the-library/library-phone-numbers.php'); ?>
          </ul>
        </div>
      </div>
    </div>

    <div>
      <div class="module-component component">
        <div class="module-heading">
          Other Contacts
        </div>

        <div class="module-content">
          <ul class="no-dots">
            <?php include_once('_partials/about-the-library/other-contacts.php'); ?>
          </ul>
        </div>
      </div>

      <div class="module-component component">
        <div class="module-heading">
          Our Mailing Address
        </div>

        <div class="module-content">
          <?php include_once('_partials/about-the-library/our-mailing-address.php'); ?>
        </div>
      </div>

      <div class="module-component component">
        <div class="module-heading">
          Follow Us
        </div>

        <div class="module-content">
          <ul class="no-dots follow-us-links">
            <?php include_once('_partials/about-the-library/follow-us.php'); ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
