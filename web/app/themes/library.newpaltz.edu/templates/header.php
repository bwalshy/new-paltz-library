<header>
  <div class="resources-container">
    <div class="resources-dropdown dropdown">
      <button class="dropdown-button">Resources <img alt=" " src="<?= get_template_directory_uri() ?>/dist/images/icons/triangle-down.svg" aria-hidden="true"/></button>
      <div class="dropdown-content">
        <?php
          if (has_nav_menu('resources_navigation')) :
            wp_nav_menu(['theme_location' => 'resources_navigation', 'menu_class' => 'resources_nav']);
          endif;
        ?>
      </div>
    </div>
  </div>

  <div class="nav-container">
    <div class="nav-header">
      <a class="logo" href="/"><img alt="NP Library Logo" src="<?= get_template_directory_uri() ?>/dist/images/new-paltz-logo.jpg"/></a>

      <button type="button" id="menu-toggle">
        <span class="sr-only">Open Navigation</span>
        <img alt=" " src="<?= get_template_directory_uri() ?>/dist/images/icons/menu-toggle.svg" aria-hidden="true"/>
      </button>
    </div>

    <nav class="nav-primary">

      <div class="search-container">
        <form role="search" method="get" class="search-form" action="<?= home_url( '/'  ); ?>">
          <label class="sr-only" for="search-this-website">Search this website:</label>
          <input id="search-this-website" type="search" class="search-field" placeholder="Search this website..." value="" name="s" title="Search for:" />
          <input type="submit" class="search-enter" value="Submit" alt="Search this website"/>
        </form>
      </div>

      <?php
        if (has_nav_menu('primary_navigation')) :
          wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'primary_nav']);
        endif;
      ?>
    </nav>

  </div>
</header>
