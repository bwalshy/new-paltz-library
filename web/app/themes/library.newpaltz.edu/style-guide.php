<?php
/**
 * Template Name: Style Guide
 */
?>

<div class="h1-container">
  <h1>H1 Lorem ipsum dolor</h1>
  <div class="help-button-container">
    <div class="help-button">
      <a href="javascript:void(0);">Help</a>
    </div>
  </div>
</div>

<h1>H1 Lorem ipsum dolor sit amet</h1>
<h2>H2 Lorem ipsum dolor sit amet, consectetur adipiscing elit. </h2>
<h3>H3 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus consectetur a neque in porttitor. </h3>
<h4>H4 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus consectetur a neque in porttitor. </h4>
<h5>H5 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus consectetur a neque in porttitor. </h5>

<p>Paragraph Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus consectetur a neque in porttitor. Paragraph Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus consectetur a neque in porttitor. Paragraph Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus consectetur a neque in porttitor. Paragraph Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus consectetur a neque in porttitor. </p>
<p>Paragraph Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus consectetur a neque in porttitor. Paragraph Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus consectetur a neque in porttitor. Paragraph Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus consectetur a neque in porttitor. Paragraph Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus consectetur a neque in porttitor. </p>

<hr>

<h2>Buttons</h2>
<div class="button">
  <a href="javascript:void(0);">Button 1</a>
</div>
<br>
<div class="button orange">
  <a href="javascript:void(0);">Orange</a>
</div>
<br>
<div class="button inverted">
  <a href="javascript:void(0);">Inverted 1</a>
</div>
<br>
<div class="button orange inverted">
  <a href="javascript:void(0);">Inverted 2</a>
</div>
<br>
<div class="button small">
  <a href="javascript:void(0);">Small</a>
</div>
<br>
<div class="button left">
  <a href="javascript:void(0);">Left</a>
</div>
  <!--
  <div class="help-button-container">
    <div class="help-button">
      <a href="javascript:void(0);">Help</a>
    </div>
  </div>
-->

<hr>

<h2>Links</h2>

<h3>Standalone Link</h3>

<a href="#" class="standalone-link">Text of Link</a>

<h3>Inline Link</h3>
<p>Paragraph Lorem ipsum dolor sit amet, consectetur adipiscing elit. <a href="javascript:void(0);">This is a text link in a paragraph.</a> Phasellus consectetur a neque in porttitor. </p>

<hr>

<h2>Lists</h2>

<p>Paragraph Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus consectetur a neque in porttitor. Paragraph Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
<ul>
  <li>List item 1</li>
  <li>List item 2</li>
  <ul>
    <li>Sub-bullet 1</li>
  </ul>
</ul>

<ol>
  <li>List item 1</li>
  <li>List item 2</li>
  <ol>
    <li>Sub-bullet 1</li>
  </ol>
</ol>

<hr>

<h2>Blockquote</h2>
<blockquote>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus consectetur a neque in porttitor. Paragraph Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus consectetur a neque in porttitor. Paragraph Lorem ipsum dolor sit amet, consectetur adipiscing elit. </blockquote>

<h2>Table</h2>

<div class="table-container">
  <table cellspacing="0" cellpadding="0"><caption>This is a table caption</caption>
    <thead>
      <tr>
        <th>Table Header 1</th>
        <th>Table Header 2</th>
        <th>Table Header 3</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td class="first">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</td>
        <td>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla.</td>
        <td>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</td>
      </tr>
      <tr>
        <td class="first">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla.</td>
        <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</td>
        <td>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</td>
      </tr>
      <tr>
        <td class="first">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</td>
        <td>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla.</td>
        <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</td>
      </tr>
      <tr>
        <td colspan="3">A row with a cell spanning all 3 columns</td>
      </tr>
    </tbody>
    <tfoot>
      <tr>
        <th>Table Footer 1</th>
        <th>Table Footer 2</th>
        <th>Table Footer 3</th>
      </tr>
    </tfoot>
  </table>
</div>

<h2>Form</h2>
<p>Note: They are a pain to style</p>

<form>
  <input type="text" placeholder="Text input"/><br><br>

  <div class="search-wrapper">
    <input type="search" placeholder="Search this website..."/>
    <input type="submit" class="search-enter" value=""/>
  </div>

  <br/>

  <div class="search-wrapper">
    <div class="select-wrapper">
      <select>
        <option>Option 1</option>
        <option>Option 2</option>
      </select>
    </div>
    <input type="search" placeholder="Search this website..."/>
    <input type="submit" class="search-enter" value=""/>
  </div>

  <div class="select-button-wrapper">
    <div class="select-wrapper">
      <select>
        <option>Option 1</option>
        <option>Option 2</option>
      </select>
    </div>
    <input type="submit" class="search-enter" value=""/>
  </div>

  <br>
  <div class="select-wrapper">
    <select>
      <option>Option 1</option>
      <option>Option 2</option>
    </select>
  </div>

  <div class="select-wrapper">
    <select class="light">
      <option>Option 1</option>
      <option>Option 2</option>
    </select>
  </div>

  <br>

  <input type="checkbox" /> Checkbox 1 <br>
  <input type="checkbox" /> Checkbox 2 <br>

  <input type="radio" /> Radio 1 <br>
  <input type="radio" /> Radio 2 <br>

  <textarea>Text area</textarea> <br>

  <input class="submit" type="submit" value="Submit"/>
</form>

<hr>

<h1>Components</h1>

<h2>Tab Box</h2>

<div class="tab-box-component component">
  <div class="tab-box">
    <div class="tab-box-menu">
      <span class="tab active" data-toggle="#tab-1">Tab 1</span>
      <span class="tab" data-toggle="#tab-2">Tab 2</span>
      <span class="tab" data-toggle="#tab-3">Tab 3</span>
      <span class="tab" data-toggle="#tab-4">Tab 4</span>

      <div class="tab-box-dropdown select-wrapper cut-overflow">
        <select class="tab-box-select" value="0">
          <option value="#tab-1">Tab 1</option>
          <option value="#tab-2">Tab 2</option>
          <option value="#tab-3">Tab 3</option>
          <option value="#tab-4">Tab 4</option>
        </select>
      </div>
    </div>
    <div class="tab-box-contents">
      <div id="tab-1" class="tab-box-content active">
        <h3>Tab 1 Content</h3>
        <p></p>
      </div>
      <div id="tab-2" class="tab-box-content">
        <h3>Tab 2 Content</h3>
        <p></p>
      </div>
      <div id="tab-3" class="tab-box-content">
        <h3>Tab 3 Content</h3>
        <p></p>
      </div>
      <div id="tab-4" class="tab-box-content">
        <h3>Tab 4 Content</h3>
        <p></p>
      </div>
    </div>
  </div>
</div>

<h2>Home Page Module</h2>

<div class="module-component component">
  <div class="module-heading">
    News Update (Standard)
  </div>

  <div class="module-content">
    <div class="placeholder"></div>
    <h3>It's Finally Here!</h3>
    <p>You’ve been waiting patiently and we are so happy to announce that the new website is finally lauched! Please enjoy being significantly less confused.</p>
  </div>
</div>

<br><br>

<div class="module-component component scrollable">
  <div class="module-heading">
    Check it Out (Scrollable)
  </div>

  <div class="module-content">
    <div class="placeholder"></div>
    <h3>It's scrollable if you want!</h3>
    <p>You’ve been waiting patiently and we are so happy to announce that the new website is finally lauched! Please enjoy being significantly less confused.</p>
    <p>You’ve been waiting patiently and we are so happy to announce that the new website is finally lauched! Please enjoy being significantly less confused.</p>
    <p>You’ve been waiting patiently and we are so happy to announce that the new website is finally lauched! Please enjoy being significantly less confused.</p>
    <p>You’ve been waiting patiently and we are so happy to announce that the new website is finally lauched! Please enjoy being significantly less confused.</p>
    <p>You’ve been waiting patiently and we are so happy to announce that the new website is finally lauched! Please enjoy being significantly less confused.</p>
  </div>
</div>

<br><br>

<h2>Availability Trackers</h2>

<div class="availability-tracker-container">
  <div class="availability-tracker" id="availability-tracker-desktop">
    <div class="availability-tracker-heading">
      <span>Desktop (80 Total)</span>
    </div>
    <div class="availability-tracker-data">
      <div class="availability-tracker-visualization">
        <div class="availability-tracker-status unavailable"></div>
        <div class="availability-tracker-status in-use"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
      </div>
      <div class="availability-tracker-numbers">
        <div class="number-available-container">
          <div class="number-available">37%</div> available
        </div>
        <div class="number-in-use-container">
          <div class="number-in-use">63%</div> in use
        </div>
      </div>
    </div>
  </div>

  <div class="availability-tracker" id="availability-tracker-laptop">
    <div class="availability-tracker-heading">
      <span>Laptop (32 Total)</span>
    </div>
    <div class="availability-tracker-data">
      <div class="availability-tracker-visualization">
        <div class="availability-tracker-status unavailable"></div>
        <div class="availability-tracker-status in-use"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
        <div class="availability-tracker-status available"></div>
      </div>
      <div class="availability-tracker-numbers">
        <div class="number-available-container">
          <div class="number-available">53%</div> available
        </div>
        <div class="number-in-use-container">
          <div class="number-in-use">47%</div> in use
        </div>
      </div>
    </div>
  </div>
</div>

<h2>Icons</h2>

<i class="icon icon-chevron-down"></i>
<i class="icon icon-triangle-down"></i>
