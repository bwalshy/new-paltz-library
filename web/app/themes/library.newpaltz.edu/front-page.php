<div id="front-page">
  <section class="welcome" style="<?php echo get_field('home_background_image') ? 'background-image:url('. get_field('home_background_image') .')' : '' ?>">
    <div class="color-overlay"></div>
    <div class="color-gradient"></div>
    <div class="container">
      <div class="welcome-text">Welcome to the Sojourner Truth Library</div>
      <p class="library-hours-text hide-text loader">
        <span>The library is</span> <span class="library-hours-status"></span> <a class="library-traffic-more-information" href="javascript:void(0);">more&nbsp;information <img src="<?= get_template_directory_uri() ?>/dist/images/icons/triangle-down.svg" aria-hidden="true" alt="Click To Expand Library Hours"/></a>
      </p>

      <?php include_once('templates/_partials/home-page/library-traffic-modal.php'); ?>
    </div>

    <div class="container">
      <div class="tab-box-component component">
        <div class="tab-box">
          <div class="tab-box-menu" role="tablist">
            <span class="tab active" data-toggle="#tab-1" role="presentation">
              <a href="javascript:void(0);" class="tab-name" aria-selected="true" role="tab" aria-controls="tab-1" >Search Our Library</a>
            </span>
            <span class="tab" data-toggle="#tab-2" role="presentation">
              <a href="javascript:void(0);" class="tab-name" aria-selected="false" role="tab" aria-controls="tab-2">Catalog</a>
            </span>
            <span class="tab" data-toggle="#tab-3" role="presentation">
              <a href="javascript:void(0);" class="tab-name" aria-selected="false" role="tab" aria-controls="tab-3">Databases</a>
            </span>
            <span class="tab" data-toggle="#tab-4" role="presentation">
              <a href="javascript:void(0);" class="tab-name" aria-selected="false" role="tab" aria-controls="tab-4">WorldCat</a>
            </span>

            <div class="tab-box-dropdown select-wrapper cut-overflow">
              <label class="sr-only" for="select-a-search">Select area of the library you wish to search:</label>
              <select id="select-a-search" class="tab-box-select" value="0">
                <option value="#tab-1">Search Our Library</option>
                <option value="#tab-2">Catalog</option>
                <option value="#tab-3">Databases</option>
                <option value="#tab-4">WorldCat</option>
              </select>
            </div>
          </div>
          <div class="tab-box-contents">
            <div id="tab-1" class="tab-box-content container active" aria-hidden="false" role="tabpanel">
              <div class="description">
                <p>Search (almost) all collections, including databases and the catalog.<br/>
                  <!--Not sure where to start? <a href="#">Search Tips</a>-->
                </p>
              </div>
              <div class="form-container">
                <form class="search-our-library-form" name="search">
                  <div class="search-wrapper">
                    <div class="select-wrapper">
                      <label class="sr-only" for="search-our-library-ebscope">Select Search Type:</label>
                      <select class="ebscope" name="ebscope" id="search-our-library-ebscope">
                       <option value="KEY">Keyword</option>
                       <option value="TI">Title</option>
                       <option value="AU">Author</option>
                     </select>
                   </div>

                   <label class="sr-only" for="search-our-library-ebterms">Enter Search Terms:</label>
                   <input type="search" class="ebterms" id="search-our-library-ebterms" name="ebterms" placeholder="Enter your search query..."/>
                   <input type="submit" class="search-enter" value="Submit"/>
                 </div>
               </form>
               <div class="button-container container">
                <div class="button small">
                  <a href="http://search.ebscohost.com/login.aspx?direct=true&site=eds-live&scope=site&type=1&authtype=guest&custid=s9001555&groupid=main" data-external="true">Advanced Search</a>
                </div>
              </div>
            </div>
          </div>
          <div id="tab-2" class="tab-box-content container" aria-hidden="true" role="tabpanel">
            <div class="description">
              <p>Search our local catalog for <!--<a href="#">-->materials we own<!--</a>--> including books, ebooks, videos and other media.</p>
            </div>
            <div class="form-container">
              <form action="https://new.sunyconnect.suny.edu:4439/F" name="search">
                <input type="hidden" name="func" value="find-b">
                <div class="search-wrapper">
                  <div class="select-wrapper">
                    <label class="sr-only" for="catalog-find_code">Select Search Type:</label>
                    <select name="find_code" id="catalog-find_code" required>
                      <option value="WRD">Word(s) Anywhere</option>
                      <option value="TTL">Title begins with...</option>
                      <option value="WTT">Title Word(s)</option>
                      <option value="AUT">Author (last name first)</option>
                      <option value="WAU">Author Word(s)</option>
                      <option value="SUB">Subject begins with...</option>
                      <option value="WSU">Subject Word(s)</option>
                      <option value="SRS">Series Title begins with...</option>
                      <option value="WSE">Series Title Word(s)</option>
                      <option value="LWT">Call Number begins with...</option>
                    </select>
                  </div>

                  <label class="sr-only" for="catalog-request">Enter Search Terms:</label>
                  <input type="search" name="request" id="catalog-request" placeholder="Enter your search query..."/>
                  <input type="submit" class="search-enter" value="Submit"/>
                </div>
              </form>
              <div class="button-container container">
                <div class="button small">
                  <a href="http://new.sunyconnect.suny.edu:4430/F/?func=find-a-0" data-external="true">Advanced Search</a>
                </div>
                <!--<div class="button small">
                  <a href="javascript:void(0);" data-external="true">Locate a Book</a>
                </div>-->
              </div>
            </div>
          </div>
          <div id="tab-3" class="tab-box-content container form-container" aria-hidden="true" role="tabpanel">
            <div class="two-column">
              <div class="column">
                <div class="description">
                  <p>Find databases by subject:</p>
                </div>
                <form action="https://newpaltz.libguides.com/az.php" name="search">
                  <div class="select-button-wrapper">
                    <div class="select-wrapper">
                      <label class="sr-only" for="databases-subject-s">Select Database:</label>
                      <select name="s" id="databases-subject-s" required>
                        <option>Select a subject</option>
                        <?php include_once('templates/_partials/home-page/tab-box-database-subjects.php'); ?>
                      </select>
                    </div>
                    <input type="submit" class="search-enter" value="Submit"/>
                  </div>
                </form>
              </div>
              <div class="column">
                <div class="description">
                  <p>Or find a specific database:</p>
                </div>
                <form action="https://newpaltz.libguides.com/az.php" name="search">
                  <div class="select-button-wrapper">
                    <div class="select-wrapper">
                      <label class="sr-only" for="databases-specific-q">Select Database:</label>
                      <select name="q" id="databases-specific-q" required>
                        <option>Select a database</option>
                        <?php include_once('templates/_partials/home-page/tab-box-database-databases.php'); ?>
                      </select>
                    </div>
                    <input type="submit" class="search-enter" value="Submit"/>
                  </div>
                </form>
              </div>
            </div>
            <div class="button-container container">
              <div class="button small">
                <a href="http://zr5qx8jp2f.search.serialssolutions.com/" data-external="true">Search Periodicals by Title</a>
              </div>
              <div class="button small">
                <a href="http://newpaltz.libguides.com/az.php" data-external="true">Go to Database Website</a>
              </div>
            </div>
          </div>
          <div id="tab-4" class="tab-box-content container" aria-hidden="true" role="tabpanel">
            <div class="description">
              <p>Search library catalogs worldwide.  Materials from other libraries can be borrowed using <a href="https://illiad.newpaltz.edu/illiad/logon.html">Interlibrary Loan</a>.</p>
            </div>
            <div class="form-container">
              <form action="https://newpaltz.worldcat.org/search" name="search">
                <input type="hidden" name="qt" value="owc_search">
                <input type="hidden" name="dblist" value="638">
                <input type="hidden" name="oldscope" value="">
                <div class="search-wrapper">
                  <div class="select-wrapper">
                    <label class="sr-only" for="worldcat-type">Select Search Type:</label>
                    <select name="scope-hidden" id="worldcat-type" required>
                      <option value="0" selected="">Libraries Worldwide</option>
                      <option value="1">SUNY New Paltz Library</option>
                    </select>
                  </div>

                  <label class="sr-only" for="worldcat-terms">Enter Search Terms:</label>
                  <input type="search" name="q" id="worldcat-terms" placeholder="Enter your search query..."/>
                  <input type="submit" class="search-enter" value="Submit"/>
                </div>
              </form>
              <div class="button-container container">
                <div class="button small">
                  <a href="http://newpaltz.worldcat.org/advancedsearch" data-external="true">Advanced Search</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php
    // include home page modules
include_once('templates/_partials/home-page-modules.php');
?>
</div>
