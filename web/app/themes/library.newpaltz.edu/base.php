<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<?php get_template_part('templates/head'); ?>
<body <?php body_class(); ?>>
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NZDLX4C"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <a class="skip-link" href="#main-content">Skip to main content</a>
    <?php
    do_action('get_header');
    get_template_part('templates/header');
    ?>
    <div class="wrap" role="document">
      <div class="content">
        <main id="main-content" class="main" role="main">
          <div class="container">
            <?php include Wrapper\template_path(); ?>
          </div>
        </main><!-- /.main -->
      </div><!-- /.content -->
    </div><!-- /.wrap -->
    <?php
    do_action('get_footer');
    get_template_part('templates/footer');
    wp_footer();
    ?>
  </body>
  </html>
