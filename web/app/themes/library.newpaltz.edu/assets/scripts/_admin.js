(function($) {
  
  /* START Notification Bar */

  // enable datetimepicker on inputs
  $('.datetimepicker').datetimepicker({
    minDate: '0'
  });

  // disable inputs if active switch is off 
  var activeSwitchSelector = '.activate-switch input';
  var $activeSwitch = $(activeSwitchSelector);
  var barActive = $activeSwitch.is(':checked');

  function toggleInputs() {
    $('.active-switch-dependent').attr('disabled', !barActive);
  }

  toggleInputs();

  // enable / disable inputs on switch toggle
  $activeSwitch.on('change', function(e) {
    barActive = !barActive;
    toggleInputs();
  });

  $(document).on('submit','#notification-bar__form', function(e){
    e.preventDefault();
  });

  // On Notification Bar title change, update the title in the preview
  $('input[name="notification_bar_name[alert_title]"]').on('keydown', function(e){
    var value = $(this).val();
    $('.notification-bar h2').text(value);
  });

  // On Notification Bar color change, update the color in the preview
  $('input[name="notification_bar_name[alert_color]"]').on('change', function(e){
    var value = $(this).val();
    $('.notification-bar').css({background: value});
  });

  // Every 3 seconds, triger a TinyMCE save, so we can 
  // get the new value and show it in the preview.
  // This is kind of hacky, but it's one of the best ways
  // to get text from TinyMCE without messing with its PHP hooks.
  setInterval(function(){
    if(window.tinyMCE) {
      tinyMCE.triggerSave();
      var html = $('textarea[name="notification_bar_name[alert_message]"]').val();
      html = html.split('&nbsp;').join('<br>');

      // Show the updated value in the preview
      $('.notification-bar p').html(html);
    }
  }, 3000);

  /* END Notification Bar */

})(jQuery);
