<?php get_template_part('templates/page', 'header'); ?>

<div class="fourohfour">
  <div class="alert alert-warning">
    <?php _e('Sorry, but the page you were trying to view does not exist.', 'sage'); ?>
  </div>
</div>

<div class="four-search">
  <?php get_search_form(); ?>
</div>
