<div id="hours-traffic">
  <div class="close-button">
    <img src="" alt="X" />
  </div>
  <div class="hours">
    <div class="heading">
      The Next 7 Days
    </div>
    <table>

    </table>
    <div class="hours-links">
      <div class="button-container container">
        <div class="button small">
          <a href="javascript:void(0);" data-external="true">Full Calendar</a>
        </div>
      </div>
      <div class="button-container container">
        <div class="button small">
          <a href="javascript:void(0);" data-external="true">Holidays and Exceptions</a>
        </div>
      </div>
    </div>
  </div>
  <div class="traffic">
    <div class="count">
      <div class="heading">
        <div>
          Traffic
        </div>
        <div class="tooltip">

        </div>
      </div>
      <div class="key">
        <div class= "swatch" id="earlier-today">
        </div>
        <div class="label">
          = earlier today
        </div>
        <div class= "swatch" id="right-now">
        </div>
        <div class="label">
          = right now
        </div>
        <div class= "swatch" id="predicted">
        </div>
        <div class="label">
          = predicted
        </div>
      </div>
      <div class="graph">
        <!-- Graph goes here -->
      </div>
      <div class="floor-select">
        <div class="floor-button">
          <div class="floor-name">
            Main Floor
          </div>
        </div>
        <div class="floor-button">
          <div class="floor-name">
            Second Floor
          </div>
        </div>
        <div class="floor-button">
          <div class="floor-name">
            Concourse
          </div>
        </div>
      </div>
    </div>
    <div class="computers">
      <div class="heading">
        Computer Availability
      </div>
      <div class="info" id="desktop">
        <div class="chart">

        </div>
        <div class="percent">
          <div class="percent-detail" id="available">
            <div class="number">

            </div>
            <div class="label">
              Available
            </div>
          </div>
          <div id="in-use">
            <div class="number">

            </div>
            <div class="label">
              Available
            </div>
          </div>
        </div>
      </div>
      <div class="info" id="laptop">
        <div class="chart">

        </div>
        <div class="percent">
          <div class="percent-detail" id="available">
            <div class="number">

            </div>
            <div class="label">
              Available
            </div>
          </div>
          <div id="in-use">
            <div class="number">

            </div>
            <div class="label">
              Available
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
