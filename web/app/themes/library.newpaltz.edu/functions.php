<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/assets.php',    // Scripts and stylesheets
  'lib/extras.php',    // Custom functions
  'lib/setup.php',     // Theme setup
  'lib/titles.php',    // Page titles
  'lib/wrapper.php',   // Theme wrapper class
  'lib/customizer.php' // Theme customizer
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);

/**
 *
 * Get link fields from a repeater
 *
 * @param (String) repeater_field_name - the acf field ID of the repeater
 * @return (array) links - the array of links in the repeater
 */
function fetch_sub_field_links($repeater_field_name) {
  $links = array();

  // if the repeater exists
  if( have_rows($repeater_field_name) ) {

    // for every row in the repeater
    while( have_rows($repeater_field_name)  ): the_row(); 

      // get the link
      $link = get_sub_field($repeater_field_name . '_links');
      
      // store it in the array
      array_push($links, $link);

    endwhile;
  }

  return $links;
}

/**
 * Turn off the notification bar
 */
function deactivate_notification_bar()
{
  // Get WP options
  $options = get_option('notification_bar_name');

  // Set active to null
  $options['active'] = null;

  // Update the options
  update_option('notification_bar_name', $options);
}

// Create action that wp_schedule_single_event will call when
// notification bar end time occurs. Look at mu-plugins/functionality/inc/alert_notice.php
add_action( 'deactivate_notification_bar', 'deactivate_notification_bar', 10, 3  );

